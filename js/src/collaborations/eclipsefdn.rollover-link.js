/*!
 * Copyright (c) 2022 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

let activeMenuItemHashId = null; // The selected menu item's #id

/**
 * Toggles the visibility of the mobile rollover / toc menu and sets the appropriate link
 * @param {Object} options
 * @param {boolean} options.visible - The visibility of the mobile rollover / toc menu.
 * @param {string} options.hashId - The id which the link will jump to. 
 */
const toggleRolloverXS = ({ visible, hashId }) => {
    const rolloverXSElement = document.querySelector('.table-of-contents-rollover-xs');

    if (!visible) {
        rolloverXSElement.style.display = 'none';
        return;
    }

    const linkElement = rolloverXSElement.querySelector('.table-of-contents-rollover-link');
    const headingElement = document.getElementById(hashId.substring(1));

    linkElement.href = hashId; 
    linkElement.textContent = headingElement.textContent;
    rolloverXSElement.style.display = 'flex';

    return;
}

const isMobile = () => { 
    const screenMdBreakpoint = 992;
    return window.innerWidth < screenMdBreakpoint
};

/*
 * Adds click event listeners to the mobile rollover / toc menu
 * A single click on a menu item will expand the menu with a 
 * display of a link along with the text of the element with the 
 * hash id corresponding to options.hashId. 
 * 
 * A second click will hide the expanded mobile menu.
 */
(async function EclipseFdnRolloverLink() {
    const elements = document.querySelectorAll('.eclipsefdn-rollover-link');

    if (elements.length === 0) return;

    elements.forEach(element => {
        const options = {
            hashId: element.dataset.hashId
        }

        if (!options.hashId) return;

        const handleClick = () => {
            if (isMobile() && activeMenuItemHashId !== options.hashId) {
                toggleRolloverXS({ visible: true, hashId: options.hashId })
                activeMenuItemHashId = options.hashId;

                return;
            }

            toggleRolloverXS({ visible: false });
            activeMenuItemHashId = null;
            window.location.hash = options.hashId;
        }

        element.addEventListener('click', handleClick);
    });

    const rolloverXSElement = document.querySelector('.table-of-contents-rollover-xs');
    rolloverXSElement.addEventListener('click', () => toggleRolloverXS({ visible: false }));
    
    // Hide rollover-xs when clicked outside of table of contents and reset state of the active menu item
    const handleOuterClick = event => {
        const isOuterClick = !tableOfContentsElement.contains(event.target);

        if (!isOuterClick) return;

        toggleRolloverXS({ visible: false });
        activeMenuItemHashId = null;
    }

    const tableOfContentsElement = document.querySelector('.collaborations-table-of-contents');
    document.addEventListener('click', handleOuterClick);

    // Hide rollover-xs when screen size changes to screen-md
    window.addEventListener('resize', () => {
        if (!isMobile() && activeMenuItemHashId) toggleRolloverXS({ visible: false });
    });
})();